QT -= core gui
TARGET = mikrolock
CONFIG += console
CONFIG -= app_bundle
TEMPLATE = app

SOURCES += ../mikrolock.c

INCLUDEPATH += ../libs ../../

#does not work:
#win32: QMAKE_CXXFLAGS += -Wl,--stack,4194304 -Wl,--heap=4194304

# get rid of mingw32 DLL dependency
win32:QMAKE_LFLAGS_WINDOWS += -static-libgcc -static-libstdc++

QMAKE_CXXFLAGS += -std=c++11 

unix:LIBS += -lm -lsodium -L.. -lmikrolock
win32 {
    DEFINES += WIN32

   LIBS += -L.. -lmikrolock

    contains(QT_ARCH, i386) {
        INCLUDEPATH += F:\libs\libsodium-win32\include
        LIBS += -LF:\libs\libsodium-win32\lib -lsodium
    } else {
        INCLUDEPATH += F:\libs\libsodium-win64\include
        LIBS += -LF:\libs\libsodium-win64\lib -lsodium
    }


    DESTDIR = ..\\..
    QMAKE_POST_LINK = $$quote(F:\upx393w\upx.exe --best F:\git\mikrolock\mikrolock.exe)
}

