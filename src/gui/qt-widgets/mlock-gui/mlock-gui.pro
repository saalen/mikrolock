#-------------------------------------------------
#
# Project created by QtCreator 2015-04-13T21:32:49
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = mikrolock-gui
TEMPLATE = app

SOURCES += main.cpp mlockmainwindow.cpp \
    showmanualdialog.cpp

HEADERS  += mlockmainwindow.h \
    showmanualdialog.h

FORMS    += mlockmainwindow.ui \
    showmanualdialog.ui

RESOURCES += mlock-gui.qrc

INCLUDEPATH += ../../../libs
INCLUDEPATH += . ../../..

QMAKE_CXXFLAGS += -std=c++11

TRANSLATIONS = mikrolock_de_DE.ts

linux {
    LIBS += -L../../.. -lmikrolock -lsodium
}

win32  {

    QT += winextras

    DEFINES += WIN32

    LIBS += -L../../.. -lmikrolock

    contains(QT_ARCH, i386) {
        INCLUDEPATH+=F:\qt-5.15.0-x32\include\QtWinExtras
        INCLUDEPATH += F:\libs\libsodium-win32\include
        LIBS += -LF:\libs\libsodium-win32\lib -lsodium
    } else {
        INCLUDEPATH+=F:\qt-5.15.0-x64\include\QtWinExtras
        INCLUDEPATH += F:\libs\libsodium-win64\include
        LIBS += -LF:\libs\libsodium-win64\lib -lsodium
    }

    DESTDIR = ..\\..\\..\\..
    RC_FILE = icon.rc

    QMAKE_POST_LINK = $$quote(E:\Devel\upx393w\upx.exe --best --force F:\git\mikrolock\mikrolock-gui.exe)
}
